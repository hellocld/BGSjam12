﻿//==============================================================================
//
// Purpose: 
//
//==============================================================================

using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


namespace Testing
{
    public class PulsePlayerArrowSystem : ComponentSystem
    {
        struct PlayerArrowEntites
        {
            public readonly int Length;
            public ComponentDataArray<PlayerArrowData> ArrowData;
            public ComponentDataArray<Scale> Scale;
        }

        [Inject] private PlayerArrowEntites m_Arrows;

        struct PlayerInputEntities
        {
            public readonly int Length;
            public ComponentDataArray<PlayerInput> Input;
        }

        [Inject] private PlayerInputEntities m_Input;

        protected override void OnUpdate()
        {
            float dt = Time.deltaTime;
            for (int i = 0; i < m_Arrows.Length; i++)
            {
                Scale tScale = m_Arrows.Scale[i];

                switch (m_Arrows.ArrowData[i].Direction)
                {
                    case ArrowDirection.UP:
                        if (m_Input.Input[0].Move.x == 1)
                        {
                            tScale.Value.xyz = new float3(1.5f, 1.5f, 1.5f);
                        }
                        break;
                    case ArrowDirection.DOWN:
                        if (m_Input.Input[0].Move.y == 1)
                        {
                            tScale.Value.xyz = new float3(1.5f, 1.5f, 1.5f);
                        }
                        break;
                    case ArrowDirection.LEFT:
                        if (m_Input.Input[0].Move.z == 1)
                        {
                            tScale.Value.xyz = new float3(1.5f, 1.5f, 1.5f);
                        }
                        break;
                    case ArrowDirection.RIGHT:
                        if (m_Input.Input[0].Move.w == 1)
                        {
                            tScale.Value.xyz = new float3(1.5f, 1.5f, 1.5f);
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                // Start scaling back to 0
                if (tScale.Value.x > 1)
                {
                    tScale.Value.x -= dt;
                }
                if (tScale.Value.y > 1)
                {
                    tScale.Value.y -= dt;
                }
                if (tScale.Value.z > 1)
                {
                    tScale.Value.z -= dt;
                }

                m_Arrows.Scale[i] = tScale;
            }
        }
    }

}
