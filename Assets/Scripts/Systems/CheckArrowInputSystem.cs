﻿//==============================================================================
//
// Purpose: 
//
//==============================================================================

using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Testing
{
    public class CheckArrowInputSystem : ComponentSystem
    {
        struct SongArrows
        {
            public readonly int Length;
            public ComponentDataArray<SongArrowData> Data;
        }

        [Inject] private SongArrows _songArrows;

        struct PlayerInputs
        {
            public readonly int Length;
            public ComponentDataArray<PlayerInput> Input;
        }

        [Inject] private PlayerInputs _inputs;

        struct MusicPlayer
        {
            public readonly int Length;
            public ComponentDataArray<MusicPlayback> Data;
        }

        [Inject] private MusicPlayer _music;

        private float PerfectDistance = 0.1f;
        private float GoodDistance = 0.3f;
        private float BadDistance = 0.5f;

        protected override void OnUpdate()
        {
            float tDistFromBeat = Mathf.Abs(_music.Data[0].CurrentBeat * _music.Data[0].TimeBetweenBeats) - GameBootstrap.GameMusic.time;
            string tCurrentArrows = SongLibrary.SongFiles[_music.Data[0].SongId].Beats[_music.Data[0].CurrentBeat];
            float4 tCurrentBeat = float4.zero;
            for (int i = 0; i < tCurrentArrows.Length; i++)
            {
                switch (tCurrentArrows[i])
                {
                    case 'U':
                        tCurrentBeat.x = 1;
                        break;
                    case 'D':
                        tCurrentBeat.y = 1;
                        break;
                    case 'L':
                        tCurrentBeat.z = 1;
                        break;
                    case 'R':
                        tCurrentBeat.w = 1;
                        break;
                }
            }

            if (tCurrentBeat.x == _inputs.Input[0].Move.x &&
                tCurrentBeat.y == _inputs.Input[0].Move.y &&
                tCurrentBeat.z == _inputs.Input[0].Move.z &&
                tCurrentBeat.w == _inputs.Input[0].Move.w &&
                tDistFromBeat <= BadDistance)
            {
                SetScore(tDistFromBeat);
            }
            else
            {
                Debug.Log("MISS!");
            }

            for (int i = 0; i < _songArrows.Length; i++)
            {
                SongArrowData tData = _songArrows.Data[i];
                if (tData.BeatIndex == _music.Data[0].CurrentBeat)
                {
                    tData.CanTap = 0;
                    _songArrows.Data[i] = tData;
                }
            }
        }

        private void SetScore(float dist)
        {
            if (dist <= PerfectDistance)
            {
                Debug.Log("Perfect!");
            } else if (dist <= GoodDistance)
            {
                Debug.Log("Good!");
            } else
            {
                Debug.Log("Bad!");
            }
        }
    }

}
