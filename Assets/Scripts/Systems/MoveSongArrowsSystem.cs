﻿//==============================================================================
//
// Purpose: 
//
//==============================================================================

using System.Collections;
using System.Collections.Generic;
using Testing;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

namespace Testing
{
    public class MoveSongArrowsSystem : JobComponentSystem
    {

        struct MoveArrowStruct : IJobProcessComponentData<SongArrowData, Position>
        {
            public float dt;
            public float songSpeed;

            public void Execute([ReadOnly] ref SongArrowData aData, ref Position aPos)
            {
                aPos.Value.y += dt * (1/songSpeed);
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new MoveArrowStruct() { dt = Time.deltaTime, songSpeed = SettingsData.timeBetweenBeats };
            return job.Schedule(this, inputDeps);
        }
    }

}
