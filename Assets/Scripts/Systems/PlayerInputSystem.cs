﻿//==============================================================================
//
// Purpose: 
//
//==============================================================================

using Unity.Entities;
using UnityEngine;


namespace Testing
{
    public class PlayerInputSystem : ComponentSystem
    {
        struct PlayerData
        {
            public readonly int Length;
            public ComponentDataArray<PlayerInput> Input;
        }

        [Inject] private PlayerData m_Players;

        protected override void OnUpdate()
        {

            for (int i = 0; i < m_Players.Length; i++)
            {
                UpdatePlayerInput(i);
            }
        }

        private void UpdatePlayerInput(int entityIndex)
        {
            PlayerInput input; // Dummy 

            input.Move.x = Input.GetButtonDown("Up") ? 1 : 0;
            input.Move.y = Input.GetButtonDown("Down") ? 1 : 0;
            input.Move.z = Input.GetButtonDown("Left") ? 1 : 0; 
            input.Move.w = Input.GetButtonDown("Right") ? 1 : 0;

            input.Select = Input.GetButtonDown("Select") ? 1 : 0;

            m_Players.Input[entityIndex] = input;
        }
    }

}
