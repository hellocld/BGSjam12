﻿//==============================================================================
//
// Purpose: 
//
//==============================================================================

using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Testing
{
    public class MusicPlaybackSystem : ComponentSystem
    {
        struct EntityGroup
        {
            public readonly int Length;
            public ComponentDataArray<MusicPlayback> Playback;
        }

        [Inject] private EntityGroup m_Music;

        protected override void OnUpdate()
        {
            MusicPlayback aPlayback = m_Music.Playback[0];
            float dt = Time.deltaTime;
            float beatDelayTime = (60/SongLibrary.SongFiles[aPlayback.SongId].Bpm) * aPlayback.StartBeatDelay;

            if (GameBootstrap.GameMusic.clip != SongLibrary.Songs[aPlayback.SongId])
            {
                GameBootstrap.GameMusic.clip = SongLibrary.Songs[aPlayback.SongId];
                GameBootstrap.GameMusic.Pause();
            }

            if (aPlayback.PlaybackStartTimer < beatDelayTime)
            {
                aPlayback.PlaybackStartTimer += dt;
            }
            else
            {
                if (!GameBootstrap.GameMusic.isPlaying)
                {
                    GameBootstrap.GameMusic.Play();
                }

                aPlayback.CurrentBeat = 
                    (int) (GameBootstrap.GameMusic.time * 
                           (SongLibrary.SongFiles[aPlayback.SongId].Bpm/60));
            }
            m_Music.Playback[0] = aPlayback;
        }

    }

}
