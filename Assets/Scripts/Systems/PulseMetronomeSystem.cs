﻿//==============================================================================
//
// Purpose: 
//
//==============================================================================

using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Testing
{
    public class PulseMetronomeSystem : JobComponentSystem
    {
        struct MetronomeJobs : IJobProcessComponentData<Metronome, Rotation, Scale>
        {
            public float dt;
            public float musicTime;
            public void Execute([ReadOnly]ref Metronome met, ref Rotation rot, ref Scale scale)
            {
                // Rotate
                rot.Value = math.mul(math.normalize(rot.Value), quaternion.eulerXYZ(dt, dt * 2, dt * 0.5f));

                // Trigger pulse check
                if (musicTime % SettingsData.timeBetweenBeats < 0.05f)
                {
                    scale.Value = new float3(2, 2, 2);
                }

                if (scale.Value.x > 1) scale.Value.x -= dt;
                if (scale.Value.y > 1) scale.Value.y -= dt;
                if (scale.Value.z > 1) scale.Value.z -= dt;
            }

        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new MetronomeJobs() {dt = Time.deltaTime, musicTime = GameBootstrap.GameMusic.time };
            return job.Schedule(this, inputDeps);
        }
        
    }

}
