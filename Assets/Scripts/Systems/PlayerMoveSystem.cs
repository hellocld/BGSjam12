﻿//==============================================================================
//
// Purpose: 
//
//==============================================================================

using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Testing
{
    public class PlayerMoveSystem : ComponentSystem
    {
        struct EntityGroup
        {
            public readonly int Length;
            public ComponentDataArray<PlayerInput> Input;
            public ComponentDataArray<Position> Position;
        }

        [Inject] private EntityGroup m_Players;

        protected override void OnUpdate()
        {
            // Since OnUpdate calls the update player function once per entity
            // per frame, just save deltaTime to a float and use that instead of
            // constantly calling Time.deltaTime
            float dt = Time.deltaTime;

            for (int i = 0; i < m_Players.Length; i++)
            {
                UpdatePlayerPosition(i, dt);
            }

        }

        private void UpdatePlayerPosition(int entityIndex, float deltaTime)
        {
            var input = m_Players.Input[entityIndex];
            var position = m_Players.Position[entityIndex];

            position.Value.x += input.Move.x * deltaTime;
            position.Value.y = 0;
            position.Value.z += input.Move.z * deltaTime;

            m_Players.Position[entityIndex] = position;
        }


    }

}
