﻿//==============================================================================
//
// Purpose: 
//
//==============================================================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Testing
{
    public static class SongLibrary
    {
        public static SongData[] SongFiles;
        public static AudioClip[] Songs;

        public static void Init()
        {
            
            List<SongData> tList = new List<SongData>();
            List<AudioClip> tClips = new List<AudioClip>();

            foreach (string songFile in System.IO.Directory.GetFiles(Application.dataPath + "\\Resources\\Song Data").
                Where(s => s.EndsWith(".json", StringComparison.OrdinalIgnoreCase)))
            {
                Debug.LogFormat("reading file {0}", songFile);
                SongData tSong = SongData.LoadFromJson(songFile);
                tList.Add(tSong);
                tClips.Add(Resources.Load<AudioClip>(tSong.SongFile));
            }

            SongFiles = tList.ToArray();
            Songs = tClips.ToArray();


        }
        
    }

}

