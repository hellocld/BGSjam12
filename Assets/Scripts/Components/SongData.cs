﻿//==============================================================================
//
// Purpose: 
//
//==============================================================================

using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;


namespace Testing
{
    [System.Serializable]
    public struct SongData
    {
        public string SongFile;
        public string Title;
        public string Artist;
        public string Length;
        public float Bpm;
        public string[] Beats;

        public static void GenerateTemplateSongFile()
        {
            SongData templateSong = new SongData
            {
                SongFile = "Song.mp3",
                Title = "Title",
                Artist = "Artist",
                Length = "3:14",
                Bpm = 120f,
                Beats = new string[]
                {
                    "U",
                    "D",
                    "L",
                    "R",
                    "UD",
                    "LR",
                    "UL",
                    "DR"
                }
            };

            string template = JsonUtility.ToJson(templateSong);

            System.IO.File.WriteAllText(Application.dataPath + "\\SongTemplate.json", template);
        }

        public static SongData LoadFromJson(string Filename)
        {
            

            return JsonUtility.FromJson<SongData>(System.IO.File.ReadAllText(Filename));

        }
    }

}
