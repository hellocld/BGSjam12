﻿//==============================================================================
//
// Purpose: 
//
//==============================================================================

using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Testing
{
    public struct MusicPlayback : IComponentData
    {
        public int StartBeatDelay;
        public int IsPlaying;
        public int SongId;
        public float PlaybackStartTimer;
        public int CurrentBeat;
        public float TimeBetweenBeats;
    }

}

