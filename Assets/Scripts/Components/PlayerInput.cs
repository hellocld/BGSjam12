﻿//==============================================================================
//
// Purpose: Component for player input data
//
//==============================================================================

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

namespace Testing
{
    public struct PlayerInput : IComponentData
    {
        public int4 Move;
        public int Select;
    }

}
