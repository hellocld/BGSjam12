﻿//==============================================================================
//
// Purpose: 
//
//==============================================================================

using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Testing
{
    public enum ArrowDirection
    {
        UP = 0,
        DOWN = 1,
        LEFT = 2,
        RIGHT = 3
    }

    public struct SongArrowData : IComponentData
    {
        public ArrowDirection Direction;
        public int BeatIndex;
        public int CanTap;
    }

    public struct PlayerArrowData : IComponentData
    {
        public ArrowDirection Direction;
    }
}
