﻿//==============================================================================
//
// Purpose: Bootstrap the ECS framework to the game
//
//==============================================================================

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Rendering;

namespace Testing
{
    public sealed class GameBootstrap
    {
        //--------------------------------------------------------------------------
        #region Members

        public static EntityArchetype PlayerArchetype;
        public static EntityArchetype MusicPlayerArchetype;
        public static EntityArchetype ArrowArchetype;
        public static EntityArchetype PlayerArrowArchetype;
        public static EntityArchetype MetronomeArchetype;

        public static MeshInstanceRenderer PlayerLook;
        public static MeshInstanceRenderer ArrowLook;
        public static MeshInstanceRenderer PlayerArrowLook;
        
        public static AudioSource GameMusic;
        public static AudioSource GameSFX;

        public static int delayBeats = 4;

        #endregion
        //--------------------------------------------------------------------------

        //--------------------------------------------------------------------------
        #region Methods


        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Initialize()
        {
            var entityManager = World.Active.GetOrCreateManager<EntityManager>();

            PlayerArchetype = entityManager.CreateArchetype(
                typeof(PlayerInput));

            MusicPlayerArchetype = entityManager.CreateArchetype(
                typeof(MusicPlayback));

            ArrowArchetype = entityManager.CreateArchetype(
                typeof(SongArrowData), typeof(Position), typeof(Rotation));

            PlayerArrowArchetype = entityManager.CreateArchetype(
                typeof(PlayerArrowData), typeof(Position), typeof(Rotation), typeof(Scale));

            MetronomeArchetype = entityManager.CreateArchetype(
                typeof(Metronome), typeof(Position), typeof(Scale), typeof(Rotation));
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void AfterSceneLoadInit()
        {
            var entityManager = World.Active.GetOrCreateManager<EntityManager>();

            // Load entity looks from scene proto prefabs
            PlayerLook = LoadLookFromSceneProto("PlayerLookProto");
            ArrowLook = LoadLookFromSceneProto("ArrowLookProto");
            PlayerArrowLook = LoadLookFromSceneProto("PlayerArrowLookProto");

            GameMusic = LoadGameAudioObject("MusicSource");
            GameSFX = LoadGameAudioObject("SFXSource");

            // Initialize the Music Library
            SongLibrary.Init();

            // Create the Player
            entityManager.CreateEntity(PlayerArchetype);
            
            // Debug
            NewGame(0);
        }

        public static void NewGame(int aSongId)
        {
            var entityManager = World.Active.GetOrCreateManager<EntityManager>();

            // Create metronome

            for (int i = 0; i < 100; i++)
            {
                var metronome = entityManager.CreateEntity(MetronomeArchetype);
                entityManager.SetComponentData(metronome, new Position
                {
                    Value = new float3(
                        UnityEngine.Random.Range(-15, 15),
                        UnityEngine.Random.Range(10, -20), 
                        UnityEngine.Random.Range(5, 35))
                });

                entityManager.SetComponentData(metronome, new Scale
                {
                    Value = new float3(1, 1, 1)
                });

                entityManager.SetComponentData(metronome, new Rotation
                {
                    Value = quaternion.eulerXYZ(
                        UnityEngine.Random.Range(0, 360),
                        UnityEngine.Random.Range(0, 360),
                        UnityEngine.Random.Range(0, 360))
                });
                
                entityManager.AddSharedComponentData(metronome, PlayerLook);

            }

            // Create the music playback entity
            ConfigureSong(entityManager, aSongId);

            CreateSongArrows(entityManager, aSongId);
            CreatePlayerArrows(entityManager);
        }

        //----------------------------------------------------------------------
        #region Utilities
        /// <summary>
        /// Finds a prototype prefab in the loaded scene used for configuring an
        /// entity's look
        /// </summary>
        /// <param name="aProtoPrefabName">Name of the scene GameObject containing the look info</param>
        /// <returns>MeshInstanceRenderer with the look we want</returns>
        private static MeshInstanceRenderer LoadLookFromSceneProto(string aProtoPrefabName)
        {
            var proto = GameObject.Find(aProtoPrefabName);
            var result = proto.GetComponent<MeshInstanceRendererComponent>().Value;
            Object.Destroy(proto);
            return result;
        }

        private static AudioSource LoadGameAudioObject(string aAudioObjectName)
        {
            var obj = GameObject.Find(aAudioObjectName);
            var result = obj.GetComponent<AudioSource>();
            return result;
        }
        #endregion
        //----------------------------------------------------------------------

        private static void CreatePlayerArrows(EntityManager aEntityManager)
        {
            var up = aEntityManager.CreateEntity(PlayerArrowArchetype);
            var down = aEntityManager.CreateEntity(PlayerArrowArchetype);
            var left = aEntityManager.CreateEntity(PlayerArrowArchetype);
            var right = aEntityManager.CreateEntity(PlayerArrowArchetype);

            aEntityManager.SetComponentData(up, new PlayerArrowData { Direction = ArrowDirection.UP});
            aEntityManager.SetComponentData(down, new PlayerArrowData { Direction = ArrowDirection.DOWN });
            aEntityManager.SetComponentData(left, new PlayerArrowData { Direction = ArrowDirection.LEFT });
            aEntityManager.SetComponentData(right, new PlayerArrowData { Direction = ArrowDirection.RIGHT });

            aEntityManager.SetComponentData(up, new Position{ Value = new float3(.5f, 0, 0)});
            aEntityManager.SetComponentData(down, new Position { Value = new float3(-0.5f, 0, 0) });
            aEntityManager.SetComponentData(left, new Position { Value = new float3(-1.5f, 0, 0) });
            aEntityManager.SetComponentData(right, new Position { Value = new float3(1.5f, 0, 0) });

            aEntityManager.SetComponentData(up, new Rotation { Value = quaternion.eulerXYZ(0, 0, 0)});
            aEntityManager.SetComponentData(down, new Rotation { Value = quaternion.eulerXYZ(0, 0, 180 * Mathf.Deg2Rad) });
            aEntityManager.SetComponentData(left, new Rotation { Value = quaternion.eulerXYZ(0, 0, 90 * Mathf.Deg2Rad) });
            aEntityManager.SetComponentData(right, new Rotation { Value = quaternion.eulerXYZ(0, 0, 270 * Mathf.Deg2Rad) });

            aEntityManager.SetComponentData(up, new Scale { Value = new float3(1, 1, 1) });
            aEntityManager.SetComponentData(down, new Scale { Value = new float3(1, 1, 1) });
            aEntityManager.SetComponentData(left, new Scale { Value = new float3(1, 1, 1) });
            aEntityManager.SetComponentData(right, new Scale { Value = new float3(1, 1, 1) });



            aEntityManager.AddSharedComponentData(up, PlayerArrowLook);
            aEntityManager.AddSharedComponentData(down, PlayerArrowLook);
            aEntityManager.AddSharedComponentData(left, PlayerArrowLook);
            aEntityManager.AddSharedComponentData(right, PlayerArrowLook);
        }

        private static void CreateSongArrows(EntityManager aEntityManager, int aSongId)
        {
            SongData tSongData = SongLibrary.SongFiles[aSongId];
            // Instantiate arrows based on songdata
            for (int beat = 0; beat < tSongData.Beats.Length; beat++)
            {
                for (int i = 0; i < tSongData.Beats[beat].Length; i++)
                {
                    Entity tArrow = aEntityManager.CreateEntity(ArrowArchetype);
                    Rotation tRot = new Rotation();
                    Position tPos = new Position();
                    tPos.Value.z = 0;
                    tPos.Value.y = -beat - delayBeats;
                    ArrowDirection tDirection = ArrowDirection.UP;
                    switch (tSongData.Beats[beat][i])
                    {
                        case 'U':
                            tDirection = ArrowDirection.UP;
                            tPos.Value.x = 0.5f;
                            break;
                        case 'D':
                            tDirection = ArrowDirection.DOWN;
                            tPos.Value.x = -0.5f;
                            tRot.Value = quaternion.eulerXYZ(0, 0, 180 * Mathf.Deg2Rad);
                            break;
                        case 'L':
                            tDirection = ArrowDirection.LEFT;
                            tPos.Value.x = -1.5f;
                            tRot.Value = quaternion.eulerXYZ(0, 0, 90 * Mathf.Deg2Rad);
                            break;
                        case 'R':
                            tDirection = ArrowDirection.RIGHT;
                            tPos.Value.x = 1.5f;
                            tRot.Value = quaternion.eulerXYZ(0, 0, 270 * Mathf.Deg2Rad);
                            break;
                    }
                    aEntityManager.SetComponentData(tArrow, new SongArrowData
                    {
                        Direction = tDirection,
                        BeatIndex = beat,
                        CanTap = 1
                    });
                    aEntityManager.SetComponentData(tArrow, tPos);
                    aEntityManager.SetComponentData(tArrow, tRot);
                    aEntityManager.AddSharedComponentData(tArrow, ArrowLook);
                }
            }

        }

        private static void ConfigureSong(EntityManager aEntityManager, int aSongId)
        {
            Entity musicPlayer = aEntityManager.CreateEntity(MusicPlayerArchetype);

            float tSongLength = SongLibrary.Songs[aSongId].length;
            float tBpm = SongLibrary.SongFiles[aSongId].Bpm;
            float tBeatTime = 60/tBpm;
            Debug.LogFormat("BeatTime: {0}", tBeatTime);
            aEntityManager.SetComponentData(musicPlayer, new MusicPlayback
            {
                StartBeatDelay = delayBeats,
                IsPlaying = 1,
                SongId = aSongId,
                PlaybackStartTimer = 0f,
                CurrentBeat = 0,
                TimeBetweenBeats = tBeatTime
            });

            GameMusic.Stop();

            SettingsData.Bpm = tBpm;
            SettingsData.timeBetweenBeats = tBeatTime;
        }

        #endregion
        //--------------------------------------------------------------------------

    }

}